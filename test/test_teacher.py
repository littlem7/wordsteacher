from wordsteacher.Teacher import Teacher
import unittest


dict_dir = '../wordsteacher/words_to_learn.csv'


class TeacherTest(unittest.TestCase):
    def setUp(self):
        self.teacher = Teacher(dict_dir)

    def test_construction_with_proper_path(self):
        teacher = Teacher(dict_dir)
        self.assertIsInstance(teacher, Teacher, msg="created object is not an instance of Teacher class")

    def test_check_words_association_correct_answer(self):
        result = self.teacher.check_words_association(0, 'emerytura')
        self.assertEquals(result, 'Correct answer!')

    def test_check_words_association_wrong_answer(self):
        result = self.teacher.check_words_association(0, 'emeryt')
        self.assertEquals(result, 'Incorrect answer! [emerytura]')

    def test_check_words_assoctiation_first_isnt_question(self):
        result = self.teacher.check_words_association(0, 'pension', False)
        self.assertEqual(result, 'Correct answer!')

    def test_give_question_proper_index(self):
        result = self.teacher.give_question(0)
        self.assertEqual(result, 'pension')

    def test_give_question_proper_index_first_isnt_question(self):
        result = self.teacher.give_question(0, False)
        self.assertEqual(result, 'emerytura')

    def test_give_question_wrong_index(self):
        result = self.teacher.give_question(7)
        self.assertEqual(result, 'Incorrect index!')

    def test_give_answer_proper_index(self):
        result = self.teacher.give_answer(0)
        self.assertEqual(result, 'emerytura')

    def test_give_answer_proper_index_first_isnt_question(self):
        result = self.teacher.give_answer(0, False)
        self.assertEqual(result, 'pension')

    def test_give_answer_wrong_index(self):
        result = self.teacher.give_answer(7)
        self.assertEqual(result, 'Incorrect index!')

    def test_give_statistic_100p_correct_answers(self):
        self.teacher.check_words_association(0, 'emerytura')
        self.teacher.check_words_association(1, 'wynagrodzenie')
        self.teacher.check_words_association(2, 'zarobki')

        result = self.teacher.give_statistic()

        self.assertEqual(result, '100.0% of correct answers!')

    def test_give_statistic_no_answers_no_statistic(self):
        result = self.teacher.give_statistic()
        self.assertEqual(result, "There's no statistics for you!")

    def test_give_statistics_50p_correct_answers(self):
        self.teacher.check_words_association(0, 'emeryt')
        self.teacher.check_words_association(1, 'wynagrodzenie')

        result = self.teacher.give_statistic()

        self.assertEqual(result, '50.0% of correct answers!')
