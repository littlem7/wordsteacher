from wordsteacher.WordsBase import WordsBase
import unittest

dict_dir = '../wordsteacher/words_to_learn.csv'


class TestWordsBase(unittest.TestCase):
    def setUp(self):
        self.base = WordsBase(dict_dir)

    def test_change_record_proper_index_works(self):
        result = self.base.change_record(0, 'ball', 'pilka')

        self.assertTrue(result)
        self.assertEqual(self.base.words_with_translation[0][0], 'ball')
        self.assertEqual(self.base.words_with_translation[0][1], 'pilka')

    def test_change_record_wrong_index(self):
        result = self.base.change_record(-1, 'ball', 'pilka')

        self.assertFalse(result)

    def test_append_record_adds_correctly(self):
        result = self.base.append_record('ball', 'pilka')
        words = self.base.words_with_translation[-1]

        self.assertTrue(result)
        self.assertEqual(words[0], 'ball')
        self.assertEqual(words[1], 'pilka')

    def test_remove_record_proper_index(self):
        result = self.base.remove_record(0)
        words_at_0_index = self.base.words_with_translation[0]

        self.assertTrue(result)
        self.assertNotEqual(words_at_0_index[0], 'pension')

    def test_remove_record_wrong_index(self):
        result = self.base.remove_record(-1)

        self.assertFalse(result)
