from wordsteacher.WordsBase import WordsBase


class Teacher:
    def __init__(self, name_of_file, association_delimiter='\n', words_delimiter=','):
        """
        Constructor creates WordsBase based on name_of_file and delimiters
        :param name_of_file: (str) full name of csv file to WordsBase
        :param association_delimiter: (str) delimiter between one word's association and another
        :param words_delimiter: (str) delimiter between words in word's association
        """
        self.base = WordsBase(name_of_file, association_delimiter, words_delimiter)
        self.num_of_correct_answers = 0
        self.num_of_mistakes = 0

    def check_words_association(self, index, answer, first_is_question=True):
        """
        Method checks user's answer with answer in base in this index and increments
        number of correct/incorrect answers if index is correct
        :param index: (int) index of word's association in WordsBase
        :param answer: (str) user's answer to check
        :param first_is_question: (boolean) True if question is first on list in WordsBase
        :return: (str) returns message [Correct answer] if answer is correct and
        [Incorrect answer! [%s]] if answer is incorrect (message with correct answer),
        method returns message [Incorrect index!] if index is incorrect.
        """
        if 0 <= index < len(self.base.words_with_translation):
            if answer.lower().strip() == self.base.words_with_translation[index][int(first_is_question)].lower().strip():
                self.num_of_correct_answers += 1
                return "Correct answer!"
            else:
                self.num_of_mistakes += 1
                return "Incorrect answer! [%s]" % (self.base.words_with_translation[index][int(first_is_question)])
        else:
            return "Incorrect index!"

    def give_question(self, index, first_is_question=True):
        """
        Method returns question in base in this index
        :param index: (int) index of word's association in WordsBase
        :param first_is_question: (boolean) True if question is first on list in WordsBase
        :return: (str) question word (or (str) message if index is incorrect)
        """
        if 0 <= index < len(self.base.words_with_translation):
            return self.base.words_with_translation[index][1 - int(first_is_question)]
        else:
            return "Incorrect index!"

    def give_answer(self, index, first_is_question=True):
        """
        Method returns answer in base in this index
        :param index: (int) index of word's association in WordsBase
        :param first_is_question: (boolean) True if question is first on list in WordsBase
        :return: (str) answer word (or (str) message if index is incorrect)
        """
        if 0 <= index < len(self.base.words_with_translation):
            return self.base.words_with_translation[index][int(first_is_question)]
        else:
            return "Incorrect index!"

    def give_statistic(self):
        """
        Method returns message with percent of correct answer in form:[x% of correct answers!]
        or message [There's no statistics for you!] there're no answer to Teacher
        :return: (str) statistic message
        """
        if self.num_of_mistakes != 0 or self.num_of_correct_answers != 0:
            sum_of_answers = self.num_of_correct_answers + self.num_of_mistakes
            return "%.1f%% of correct answers!" % (self.num_of_correct_answers/sum_of_answers*100)
        else:
            return "There's no statistics for you!"
