import csv


class WordsBase:
    def __init__(self, name_of_file, association_delimiter='\n', words_delimiter=','):
        """
        Constructor creates WordsBase based on name_of_file and delimiters
        :param name_of_file: (str) full name of csv file to WordsBase
        :param association_delimiter: (str) delimiter between one word's association and another
        :param words_delimiter: (str) delimiter between words in word's association
        """
        self.words_with_translation = []
        with open(name_of_file, 'r') as csvfile:
            data = csv.reader(csvfile, delimiter=association_delimiter)
            for row in data:
                new_foreign_word = row[0].split(words_delimiter)[0]
                new_translated_word = words_delimiter.join(row[0].split(words_delimiter)[1:])
                self.words_with_translation.append([new_foreign_word, new_translated_word])

    def change_record(self, index, new_foreign_word, new_translated_word):
        """
        Method changes record with index of word's association in WordsBase
        :param index: (int) index of word's association in WordsBase
        :param new_foreign_word: (str) word to replace with foreign word in WordsBase
        :param new_translated_word: (str) word to replace with translated word in WordsBase
        :return: True if both of words aren't empty strings and index isn't out of range else False
        """
        if new_foreign_word != '' and new_translated_word != '' and 0 <= index < len(self.words_with_translation):
            self.words_with_translation.remove(self.words_with_translation[index])
            self.words_with_translation.insert(index, [new_foreign_word, new_translated_word])
            return True
        else:
            return False

    def append_record(self, new_foreign_word, new_translated_word):
        """
        Method appends record to a WordsBase
        :param new_foreign_word: (str) foreign word to append to WordsBase
        :param new_translated_word: (str) translated word to append to WordsBase
        :return:True if both of words aren't empty strings and index isn't out of range else False
        """
        if new_foreign_word != '' and new_translated_word != '':
            self.words_with_translation.append([new_foreign_word, new_translated_word])
            return True
        else:
            return False

    def remove_record(self, index):
        """
        Method removes record with index of word's association in WordsBase
        :param index: (int) index of word's association in WordsBase
        :return: True if index isn't out of range
        """
        if 0 <= index < len(self.words_with_translation):
            self.words_with_translation.remove(self.words_with_translation[index])
            return True
        else:
            return False
