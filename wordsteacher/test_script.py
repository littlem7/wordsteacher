from wordsteacher.Teacher import Teacher


if __name__ == "__main__":
    # init for Teacher and WordsBase
    my_teacher = Teacher('words_to_learn.csv')

    # example changes of WordsBase mathods
    print("Let's make some changes")
    my_teacher.base.append_record("cello", "wiolączela")
    my_teacher.base.change_record(len(my_teacher.base.words_with_translation) - 1, "cello", "wiolonczela")
    my_teacher.base.remove_record(len(my_teacher.base.words_with_translation) - 2)

    # example action of Teacher methods
    print("Let's start the lessons")
    for index in range(len(my_teacher.base.words_with_translation)):
        print(my_teacher.give_question(index, first_is_question=False))
        print(my_teacher.check_words_association(index, input(" -> "), first_is_question=False))
    print(my_teacher.give_statistic())
